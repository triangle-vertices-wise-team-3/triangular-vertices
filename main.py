#triangle-vertices-project
# importing exit function from sys module
from sys import exit
# constructing triangular grid
def triangle_grid():
    number = int(input("Enter number of rows : "))
    val = 0
    for i in range(number):
        for j in range((number-i)-1):
            space()
        for j in range (i+1):
                val+=1
                print(format(val,"<4"),end=" ")
        print()


# function to get spaces 
def space():
     print(end="  ")

#checking if it is traingular or not

def tri(a):
    l=abs(m[0][2]-m[1][2])
    r=a[1]-a[0];
    s=a[2]-a[1];
    q=a[2]-a[0];
    if r>=s: 
        b=abs(m[0][0]-a[0])
        c=abs(m[1][0]-a[1])
        q=s
    else:
        b=abs(m[0][0]-a[0])
        c=abs(m[1][0]-a[2])
        q=r    
    if q==l and (b==c or b+l==c):
        for i in h:
            print(i,sep=' ',end=' ')
        print("are the vertices of a triangle") 
    else:
        not_accept(h)  



def parallelogram(a, m):
    l = abs(m[0][2] - m[1][2])
    b = abs(m[0][0] - a[0])
    c = abs(m[1][0] - a[2])
    p = a[1] - a[0]
    q = a[3] - a[2]
    r = a[2] - a[0]
    s = a[3] - a[1]
    if p == q and r == s and p == l and (b == c or b + l == c):
        print(*h, sep=' ', end=' ')
        print("are the vertices of a parallelogram")
    else:
        not_accept(h)

        
def hexagon(a, m):
    b = abs(m[0][2] - m[1][2])
    c = abs(m[1][2] - m[2][2])
    d = a[1] - a[0]
    e = a[5] - a[4]
    f = a[3] - a[2]
    if d == e and f == e + b and e == c:
        for i in h:
           print(i, sep=' ', end=' ')
        print("are the vertices of a hexagon")
    else:
        not_accept(h)

        
def not_accept(a):
    for i in a:
        print(i, sep=' ', end=' ')
    print("are not the vertices of an acceptable figure")


# collecting data related  to input points given and manupulating them
def max_min_len(h):
    h.sort()
    start = 1;
    end = 1;
    k = 0;
    row_no = 0
    m = []
    while (True):
        while (True):
            if a[k] >= start and a[k] <= end:
                if row_no == 0:
                    p = [start, end, row_no + 1]
                else:
                    p = [start, end, row_no]
                if p not in m:
                    m.append(p)
                break
            if row_no == 0:
                start = start + row_no
                end = end + row_no
            else:
                start = start + row_no
                end = end + row_no + 1
            row_no = row_no + 1
        k = k + 1
        if len(a) == k:
            break
    return (m)


#drawing trianglular grid 
triangle_grid()
#taking inputs from the user
while(True):
    h=list(map(int,input().split()))
    if(h==[]):
        exit(0)
    a=[]
    for i in h:
        a.append(i) 

 # checking if it is an acceptable figure or not 
    if len(a)==3:
         m=max_min_len(a)
         if len(m)==2:
             tri(a)
         else: 
              not_accept(h) 
    elif len(a) == 4:
        m = max_min_len(a)
        if len(m) == 2:
            parallelogram(a, m)
        else:
            not_accept(h)
    elif len(a) == 6:
        m = max_min_len(a)
        if len(m) == 3:
            hexagon(a, m)
        else:
            not_accept(h)
    else:
        not_accept(h)
     
